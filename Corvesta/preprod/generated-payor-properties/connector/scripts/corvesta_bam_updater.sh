#!/bin/bash

HE_DIR=$1
HRC_USER=$2
HRC_HOST=$3

echo "************************************** CORVESTA SH : corvesta_bam_updater.sh START **************************************"

if [[ ! -d $HE_DIR/last-run-json ]] ; then
    echo "$HE_DIR/last-run-json directory not found!!!"
    exit 0
fi

cd $HE_DIR/last-run-json

jsons_to_set=($(grep -rl jobName))

for i in "${jsons_to_set[@]}"
do
    echo "----------------------------------------------------------------------------------------------------------------------------"
    echo "Setting hrc:last-run-entry using JSON: '$i' with below range:"
    echo `grep  'jobName' $i`

    if [[ $i = "connector-events-last-run-PremiumPayableAllocationEvents-Complete.json" || $i = "corvesta-last-run-provider-taxentity-reinstate-outbound.json" || $i = "connector-events-funding-last-run-FundingAdjRequestEvents-Complete.json" ]]; then
      CURR_TIMESTAMP=`date +"%Y-%m-%dT%H:%M:%S.%3N"`
      sed -i -e "s/\([0-9][0-9][0-9][0-9]\)-\([0-9][0-9]\)-\([0-9][0-9]\)T\([0-9][0-9]\):\([0-9][0-9]\):\([0-9][0-9]\)\.\([0-9][0-9][0-9]\)/${CURR_TIMESTAMP}/g" $i
    else
      sed -i -e "s/\([0-9][0-9][0-9][0-9]\)-\([0-9][0-9]\)-\([0-9][0-9]\)T\([0-9][0-9]\):\([0-9][0-9]\):\([0-9][0-9]\)\.\([0-9][0-9][0-9]\)/2020-03-25T11:00:00.000/g" $i
    fi

    echo `grep  'startTime' $i`
    echo `grep  'endTime' $i`

    ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST last-run-entry file:last-run-json//$i
    echo "----------------------------------------------------------------------------------------------------------------------------"
done

echo "************************************** CORVESTA SH : corvesta_bam_updater.sh END **************************************"
