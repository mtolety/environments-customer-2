#!/bin/bash

prefix=L
suffixCount=3
idSeparator=-
payorUser=weblogic
payorHosts=172.29.36.149,172.29.36.150
weblogicDomainDir=/home/weblogic/oracle/12.2.1.4.0/user_projects/domains/config
weblogicJarsDir=/home/weblogic/oracle/12.2.1.4.0/user_projects/domains/config/jars

export IFS=","
for payorHost in $payorHosts; do

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#PREFIX#|${prefix}|g' ${weblogicJarsDir}/IDGeneratorConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#SUFFIX_COUNT#|${suffixCount}|g' ${weblogicJarsDir}/IDGeneratorConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ID_SEPARATOR#|${idSeparator}|g' ${weblogicJarsDir}/IDGeneratorConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ROLE_ID_SEPARATOR#|${idSeparator}|g' ${weblogicJarsDir}/IDGeneratorConfig.properties"

done

exit 0
